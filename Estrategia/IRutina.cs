﻿using AppEjerciciosT3.DB;
using AppEjerciciosT3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppEjerciciosT3.Estrategia
{

    //ESTRATEGIA
    public interface IRutina
    {
        public void AsignarEjercicios(Rutina rutina, AppEjerciciosT3Context context); 

    }

}
