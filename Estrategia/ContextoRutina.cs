﻿using AppEjerciciosT3.DB;
using AppEjerciciosT3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppEjerciciosT3.Estrategia
{
    public class ContextoRutina
    {
        private IRutina _rutina;


        public ContextoRutina()
        {        
        }

        public void asignarEstrategia(IRutina rutina)
        {
            this._rutina = rutina;
        }

        public void GenerarRutina(Rutina rutina,AppEjerciciosT3Context _context)
        {
            _context.Add(rutina);
            _context.SaveChanges();
            this._rutina.AsignarEjercicios(rutina,_context);
        }
    }
}
