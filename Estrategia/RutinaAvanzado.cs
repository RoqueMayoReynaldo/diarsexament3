﻿
using AppEjerciciosT3.DB;
using AppEjerciciosT3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppEjerciciosT3.Estrategia
{
    public class RutinaAvanzado : IRutina

    {
        public void AsignarEjercicios(Rutina rutina, AppEjerciciosT3Context context)
        {


            //random
            Random r = new Random();

            //Ejercicios disponibles

            List<Ejercicio> ejercicios = context.Ejercicios.ToList();

            //For para la cantidad de ejercicios for the rutine
            for (int i = 0; i < 15; i++)
            {
                EjercicioRutina ejercicioRutina = new EjercicioRutina();
                ejercicioRutina.idRutina = rutina.id;
                ejercicioRutina.tiempo = 120;
                ejercicioRutina.idEjercicio = ejercicios[r.Next(ejercicios.Count)].id;

                context.EjercicioRutinas.Add(ejercicioRutina);
                context.SaveChanges();
            }



        }
    }
}
