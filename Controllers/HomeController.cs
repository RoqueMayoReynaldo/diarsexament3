﻿using AppEjerciciosT3.DB;
using AppEjerciciosT3.Estrategia;
using AppEjerciciosT3.Factory;
using AppEjerciciosT3.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace AppEjerciciosT3.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public static ContextoRutina contextoRutina;

        private AppEjerciciosT3Context context;

        public HomeController(AppEjerciciosT3Context context)
        {
            this.context = context;
        }


        public IActionResult Index()
        {

            

            
            return View();
        }

        [HttpGet]
        public IActionResult CrearRutina()
        {




            return View();
        }





        [HttpPost]
        public IActionResult CrearRutina(Rutina rutina,String nivelRutina)
        {

            if (ModelState.IsValid)
            {
                var claim = HttpContext.User.Claims.First();
                string username = claim.Value;

                Usuario user = context.Usuarios.FirstOrDefault(o=>o.usuario==username);
                rutina.idUsuario = user.id;



                //HACIENDO USO DE ESTRATEGIA Y ADICIONALMENTE FACTORY METHOD 
                

                FabricaRutina fabrica = new FabricaRutina();

                ContextoRutina contextoRutina = new ContextoRutina();
                contextoRutina.asignarEstrategia(fabrica.fabricarRutina(nivelRutina));
                contextoRutina.GenerarRutina(rutina,this.context);

                return RedirectToAction("Index");

            }

           return View();
        }

        [HttpGet]
        public IActionResult MisRutinas()
        {
            var claim = HttpContext.User.Claims.First();
            string username = claim.Value;

            Usuario user = context.Usuarios.Include("rutinas.ejericioRutinas.ejercicio").FirstOrDefault(o => o.usuario == username);

            


            return View(user);
        }


        [HttpGet]

        public IActionResult Logout()
        {
           

            HttpContext.SignOutAsync();

            return RedirectToAction("Ingreso", "Autenticacion");
        }





    }
}
