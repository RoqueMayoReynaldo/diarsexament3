﻿using AppEjerciciosT3.DB;
using AppEjerciciosT3.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AppEjerciciosT3.Controllers
{
    public class AutenticacionController : Controller
    {


        private AppEjerciciosT3Context context;

        public AutenticacionController(AppEjerciciosT3Context context)
        {
            this.context = context;
        }


        [HttpGet]
        public ActionResult Ingreso()
        {
          
            return View();
        }
        [HttpPost]
        public IActionResult Ingreso(String usuario,String clave)
        {

            Usuario userN = context.Usuarios.FirstOrDefault(Usuario => Usuario.usuario == usuario && Usuario.clave == clave);

            if (userN == null)
            {
                ModelState.AddModelError("Mensaje", "Porfavor verifique sus datos de ingreso");
            }

            if (ModelState.IsValid)
            {

                var claims = new List<Claim> {
                new Claim(ClaimTypes.Name,usuario)
              };

                var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);

                HttpContext.SignInAsync(claimsPrincipal);


                return RedirectToAction("Index", "Home");
            }
         

            return View();
        }


        [HttpPost]
        public IActionResult Registro(Usuario nuevoUsuario)
        {

            if (ModelState.IsValid)
            {
                context.Usuarios.Add(nuevoUsuario);
                context.SaveChanges();
                return RedirectToAction("Ingreso");

            }


        
            return View("Ingreso");
        }


    }
}
