﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppEjerciciosT3.Models
{
    public class EjercicioRutina
    {
        public int id { get; set; }
        public int tiempo { get; set; }
        public int idRutina { get; set; }
        public int idEjercicio { get; set; }
        public Ejercicio ejercicio { get; set; }
    }
}
