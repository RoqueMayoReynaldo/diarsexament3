﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AppEjerciciosT3.Models
{
    public  class Rutina
    {
        public int id { get; set; }
        [Required(ErrorMessage = "Nombre para rutina requerido")]
        public String nombre { get; set; }

        public int idUsuario { get; set; } 

        public List<EjercicioRutina> ejericioRutinas { get; set; }
    }
}
