﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppEjerciciosT3.Models
{
    public class Ejercicio
    {
        public int id { get; set; }
        public String nombre { get; set; }
        public String video { get; set; }

    }
}
