﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AppEjerciciosT3.Models
{
    public class Usuario
    {
        public int id { get; set; }
        [Required(ErrorMessage = "Nombre es requerido")]
        public String nombre { get; set; }
        [Required(ErrorMessage = "La edad es requerida")]
        [Range(5, 150,ErrorMessage ="Edad permitida 5-150")]
        public int edad { get; set; }
        [Required(ErrorMessage = "Usuario es requerido")]
        [MinLength(4, ErrorMessage = "Se requiere almenos 4 caracteres")]
        public String usuario { get; set; }
        [Required(ErrorMessage = "Clave es requerida")]
        [MinLength(6, ErrorMessage = "Se requiere almenos 6 caracteres")]
        public String clave { get; set; }

        public List<Rutina> rutinas { get; set; }
    }
}
