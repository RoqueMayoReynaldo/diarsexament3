﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppEjerciciosT3.Models;

namespace AppEjerciciosT3.DB.Mapping
{
    public class RutinaMap : IEntityTypeConfiguration<Rutina>

    {
        public void Configure(EntityTypeBuilder<Rutina> builder)
        {


            builder.ToTable("Rutina", "dbo");
            builder.HasKey(Rutina => Rutina.id);
            builder.HasMany(Rutina=>Rutina.ejericioRutinas).WithOne().HasForeignKey(o=>o.idRutina);


        }
    }
}
