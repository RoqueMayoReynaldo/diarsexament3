﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppEjerciciosT3.Models;

namespace AppEjerciciosT3.DB.Mapping
{
    public class UsuarioMap : IEntityTypeConfiguration<Usuario>

    {
        public void Configure(EntityTypeBuilder<Usuario> builder)
        {


            builder.ToTable("Usuario", "dbo");
            builder.HasKey(Usuario => Usuario.id);
            builder.HasMany(Usuario=>Usuario.rutinas).WithOne().HasForeignKey(o=>o.idUsuario);


        }
    }
}
