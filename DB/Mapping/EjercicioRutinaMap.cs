﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AppEjerciciosT3.Models;

namespace AppEjerciciosT3.DB.Mapping
{
    public class EjercicioRutinaMap : IEntityTypeConfiguration<EjercicioRutina>

    {
        public void Configure(EntityTypeBuilder<EjercicioRutina> builder)
        {


            builder.ToTable("EjercicioRutina", "dbo");
            builder.HasKey(EjercicioRutina => EjercicioRutina.id);
            builder.HasOne(EjercicioRutina=>EjercicioRutina.ejercicio).WithMany().HasForeignKey(o=>o.idEjercicio);


        }
    }
}
