﻿using AppEjerciciosT3.Estrategia;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppEjerciciosT3.Factory
{
    //CREADOR
    public   class FabricaRutina
    {
        public virtual  IRutina fabricarRutina(String tipo)
        {

            switch (tipo.ToLower())
            {
                case "principiante": return new RutinaPrincipiante();
                case "intermedio": return new RutinaIntermedio();
                case "avanzado": return new RutinaAvanzado();

                default:
                    throw new ArgumentException("Tipo desconocido");
            }
        }
    }
}
